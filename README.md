# CURSO: Marketing Digital 2021

Link acceso a las clases: https://meet.google.com/evz-mykt-dqz

**Temario**

[[_TOC_]]

## Módulo 4: Creación y manejo de cuentas en redes sociales

### Clase 1: Oportunidades/Exito Online
- [Diapositiva](https://gitlab.com/ciorozco/2021-marketing-digital-4.0/-/blob/master/Recursos/M4_Clase_1.pdf) <br>
- [Video - Comisión 1](https://www.youtube.com/watch?v=7vxTsc79oDA) <br>
- [Video - Comisión 2](https://www.youtube.com/watch?v=AmIJkiRvR_U) <br>
- [Video - Comisión 3](https://www.youtube.com/watch?v=tMmKGwfBRsg) <br>
- [Video - Comisión 4](https://www.youtube.com/watch?v=Q0x6r6gk1Nk) <br>
- [Video - Comisión 5 - Parte 1](https://www.youtube.com/watch?v=AtOZYHOsRr0) <br>

### Clase 2: Presencia Online
- [Diapositiva](https://gitlab.com/ciorozco/2021-marketing-digital-4.0/-/blob/master/Recursos/M4_Clase_2.pdf) <br>
- [Video - Comisión 1](https://www.youtube.com/watch?v=U3tJB3cyJxE) <br>
- [Video - Comisión 2](https://www.youtube.com/watch?v=v4iC1Mbgh3U) <br>
- [Video - Comisión 3](https://www.youtube.com/watch?v=rviclg_6hlo) <br>
- [Video - Comisión 4](https://www.youtube.com/watch?v=-YzxVhGqPnI) <br>
- [Video - Comisión 5 - Parte 2](https://www.youtube.com/watch?v=MAmzqe30ZT4) <br>

### Clase 3:
- [Video - Comisión 1](https://www.youtube.com/watch?v=1tXfK_ioXGs) <br>
- [Video - Comisión 2](https://www.youtube.com/watch?v=2hxwnyPlKw4) <br>
- [Video - Comisión 3](https://www.youtube.com/watch?v=buIKpzk8IQI) <br>
- [Video - Comisión 4](https://www.youtube.com/watch?v=1gXOqfFncSg) <br>
- [Video - Comisión 5 - Parte 1](https://www.youtube.com/watch?v=G2fS2OtEanQ) <br>

### Clase 4:
- [Video - Comisión 1](https://www.youtube.com/watch?v=6GBQyuibDfo) <br>
- [Video - Comisión 2](https://www.youtube.com/watch?v=fEGsVGIt2kk) <br>
- [Video - Comisión 3](https://www.youtube.com/watch?v=sD1NKglDzew) <br>
- [Video - Comisión 4](https://www.youtube.com/watch?v=XYnbYdDKLak) <br>
- [Video - Comisión 5 - Parte 2](https://www.youtube.com/watch?v=GhyiXbfy_rI) <br>

## Módulo 5: De la estrategia a la ejecución: principales herramientas


